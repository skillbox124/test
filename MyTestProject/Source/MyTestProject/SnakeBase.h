// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;
UENUM()
enum class EMovementDiretcion
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class MYTESTPROJECT_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	TArray<AActor*> FoodTypes;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	EMovementDiretcion LastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
	float Speed;

	UPROPERTY()
	int Score;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum);

	void Move();

	UFUNCTION()
	void SnakeElementOverlap (ASnakeElementBase* OverlappedElement, AActor* Other);

	void AddFoodSnakeElement (int Elements);

	void ChangeSpeed (float NewSpeed);

	void SetDefaultSpeed ();

	void InrecreaseScore (int value);

	void FoodSpawner ();
};
