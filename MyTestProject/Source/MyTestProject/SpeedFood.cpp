// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedFood.h"
#include "SnakeBase.h"

// Sets default values
ASpeedFood::ASpeedFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

//����� ������������ ����������� �������� ���� SpeedUpFood ����������������� � ������� ������
//�������� ����� ChangeSpeed
//������� ����������� ���������
//��������� ����
//������������
void ASpeedFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake-> ChangeSpeed(0.1f);
			GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("SpeedUp"));
			Snake->InrecreaseScore(1);
		}
	}
	Destroy();
}