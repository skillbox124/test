// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePlayerPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
ASnakePlayerPawn::ASnakePlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//�������� �������� ���������� - ������ � �������� ��� � RootComponent 
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void ASnakePlayerPawn::BeginPlay()
{
	Super::BeginPlay();

	//��������� ������� ������ ��� ������ ����
	SetActorRotation(FRotator(-90, 0, 0));
	
	CreateSnakeActor();
}

// Called every frame
void ASnakePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//���������� �������� Input, W/S ��� �����/���� � A/D ��� �����/������
	PlayerInputComponent -> BindAxis("Vertical", this, &ASnakePlayerPawn::HandlePlayerVerticalInput);
	PlayerInputComponent -> BindAxis("Horizontal", this, &ASnakePlayerPawn::HandlePlayerHorizontalInput);


}

void ASnakePlayerPawn::CreateSnakeActor()
{
	//����� ������ ������������� SnakeActorClass ������ ASnakeBase
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	//SnakeActor->SetActorLocation( FVector(0, 0, 75));
}

//����� ��������� ���������� ������ � �������, �������� ����������� ��������
//� �� ����������� �������� ��� ����������� �� ��������������� (��� W/S)
void ASnakePlayerPawn::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor ->LastMoveDirection != EMovementDiretcion::DOWN)
		{
			SnakeActor->LastMoveDirection = EMovementDiretcion::UP;
		}

		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDiretcion::UP)
		{
			SnakeActor->LastMoveDirection = EMovementDiretcion::DOWN;
		}
	}
}


void ASnakePlayerPawn::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDiretcion::RIGHT)
		{
			SnakeActor -> LastMoveDirection = EMovementDiretcion::LEFT;
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDiretcion::LEFT)
		{
			SnakeActor -> LastMoveDirection = EMovementDiretcion::RIGHT;
		}
	}
}

