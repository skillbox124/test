// Fill out your copyright notice in the Description page of Project Settings.


#include <ctime>
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "SpeedFood.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 55.f;
	Speed = 0.5f;
	LastMoveDirection = EMovementDiretcion::DOWN;
	Score = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();



	//��������� �������� ������, � ������ ������ ����� Tick
	SetActorTickInterval(Speed);

	//��������� �������� ������
	AddSnakeElement(4);
}

// Called every frame
//�������� ����� Move ������ Tick
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();	

}

//����� ���������� ������ �������� ������ � ����������� �� �������� � ElementsNum, ����������� ��� � Snake,
//���� ��� ������� ������� � �������, �� �� ���������� �������
void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 30);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass,NewTransform);
		int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
		NewSnakeElem->SnakeOwner = this;

		if (ElementIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}

		
	}
}

//����� Move � ����������� �� �������� ENUM EMovementDirection �������� ����������� �������� ������
void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	float MoveSpeed = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDiretcion::UP:
		MovementVector.X += MoveSpeed;
		break;
	case EMovementDiretcion::DOWN:
		MovementVector.X -= MoveSpeed;
		break;
	case EMovementDiretcion::RIGHT:
		MovementVector.Y += MoveSpeed;
		break;
	case EMovementDiretcion::LEFT:
		MovementVector.Y -= MoveSpeed;
		break;
	}

	//AddActorWorldOffset(MovementVector);

	SnakeElements[0] -> ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		ASnakeElementBase* CurrentSnakeElement = SnakeElements[i];
		ASnakeElementBase* PreviousSnakeElement = SnakeElements[i - 1];
		FVector PreviousLocation = PreviousSnakeElement -> GetActorLocation();
		CurrentSnakeElement -> SetActorLocation (PreviousLocation);
	}

	SnakeElements[0] -> AddActorWorldOffset (MovementVector);
	SnakeElements[0] -> ToggleCollision();
}

// ����� ��������� ����� ������� ������ ��������������� � ������ ��������� ����, ���� ��� ���� ������ �� �������� ����� ��������
void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex== 0;

		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

// ����� ��������� � ������ ����� ������� ���� ��� ����������������� � Food
void ASnakeBase::AddFoodSnakeElement(int Elements)
{
	auto LastElementLocation = SnakeElements.Last()->GetActorLocation();
	FVector Change(ElementSize, 0, 0);
	FVector NewLocation (LastElementLocation * Change);
	FTransform LastElementTransform (NewLocation);
	ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, LastElementTransform);
	int32 ElementIndex = SnakeElements.Add(NewSnakeElem);
	NewSnakeElem->SnakeOwner = this;

}

//����� �������� �������� ������ ���� ��� ����������������� � SpeedUpFood � ����� �������� ����� 30 ������ ����� SetDefaltSpeed
void ASnakeBase::ChangeSpeed(float NewSpeed)
{
	SetActorTickInterval(0.3f);
	FTimerHandle Handle; 
	GetWorld()->GetTimerManager().SetTimer(Handle,this, &ASnakeBase::SetDefaultSpeed, 30.0f, false);
}

//����� ���������� ������ ������� ��������, ���������� ������� ChangeSpeed ����� 30 ������ ����� ��� ���������
void ASnakeBase::SetDefaultSpeed()
{
	SetActorTickInterval(0.5f);
	GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("SpeedBackToNormal"));
}

//����� ������� ��������� �������� ���� ���� ������ ����������������� � �������� ������� ������� ���������� �����
void ASnakeBase::InrecreaseScore(int value)
{
	Score = Score + value;
}

//����� ������� ����� Food � ��������� ����� � ������ ������� ����
void ASnakeBase::FoodSpawner()
{
	int X_Axis  = 0;
	int Y_Axis = 0;
	int Z_Axis = 30;
	int FoodSwitcher = 1;
	int OddNumbresGenerator = 0;
//��������� ���������, � �� ��������������� �����
	srand(time(NULL));
//������ ��� ���������� ���������� ������� ����� ���������, ������������� ��� ������������� �������� ������ ���������� �
	OddNumbresGenerator = rand()%10;
//���� ���������� �������� ������ 4, �� ����� ���������� ����� � ������������� ��� �/ ����� ����� � �������������
	if (OddNumbresGenerator > 4)
	{
		X_Axis = rand()%17;
	}
	else
	{
		X_Axis = (rand() % 17);
		X_Axis *= -1;
	}
//�� �� �������� �� ��� ���������� Y
	OddNumbresGenerator = rand()%10;

	if (OddNumbresGenerator > 4)
	{
		Y_Axis = rand()%17;
	}
	else
	{
		Y_Axis = (rand()%17);
		Y_Axis *= -1;
	}
//���������� ����� Food ����� ����������, ���� ��� ������ �������
	if (FoodSwitcher == 1)
	{
		FVector DeltaFoodLocation (ElementSize * X_Axis, ElementSize * Y_Axis, Z_Axis);
		FTransform NewFoodLocation (DeltaFoodLocation);
		AFood* Food = GetWorld()->SpawnActor<AFood>(FoodClass,NewFoodLocation);
	}
}

