// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYTESTPROJECT_SnakePlayerPawn_generated_h
#error "SnakePlayerPawn.generated.h already included, missing '#pragma once' in SnakePlayerPawn.h"
#endif
#define MYTESTPROJECT_SnakePlayerPawn_generated_h

#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_SPARSE_DATA
#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakePlayerPawn(); \
	friend struct Z_Construct_UClass_ASnakePlayerPawn_Statics; \
public: \
	DECLARE_CLASS(ASnakePlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTestProject"), NO_API) \
	DECLARE_SERIALIZER(ASnakePlayerPawn)


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakePlayerPawn(); \
	friend struct Z_Construct_UClass_ASnakePlayerPawn_Statics; \
public: \
	DECLARE_CLASS(ASnakePlayerPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTestProject"), NO_API) \
	DECLARE_SERIALIZER(ASnakePlayerPawn)


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakePlayerPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakePlayerPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakePlayerPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakePlayerPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakePlayerPawn(ASnakePlayerPawn&&); \
	NO_API ASnakePlayerPawn(const ASnakePlayerPawn&); \
public:


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakePlayerPawn(ASnakePlayerPawn&&); \
	NO_API ASnakePlayerPawn(const ASnakePlayerPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakePlayerPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakePlayerPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakePlayerPawn)


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_PRIVATE_PROPERTY_OFFSET
#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_12_PROLOG
#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_PRIVATE_PROPERTY_OFFSET \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_SPARSE_DATA \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_RPC_WRAPPERS \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_INCLASS \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_PRIVATE_PROPERTY_OFFSET \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_SPARSE_DATA \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_INCLASS_NO_PURE_DECLS \
	MyTestProject_Source_MyTestProject_SnakePlayerPawn_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYTESTPROJECT_API UClass* StaticClass<class ASnakePlayerPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyTestProject_Source_MyTestProject_SnakePlayerPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
