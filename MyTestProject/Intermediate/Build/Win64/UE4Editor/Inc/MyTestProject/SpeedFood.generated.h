// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYTESTPROJECT_SpeedFood_generated_h
#error "SpeedFood.generated.h already included, missing '#pragma once' in SpeedFood.h"
#endif
#define MYTESTPROJECT_SpeedFood_generated_h

#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_SPARSE_DATA
#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_RPC_WRAPPERS
#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpeedFood(); \
	friend struct Z_Construct_UClass_ASpeedFood_Statics; \
public: \
	DECLARE_CLASS(ASpeedFood, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTestProject"), NO_API) \
	DECLARE_SERIALIZER(ASpeedFood) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedFood*>(this); }


#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASpeedFood(); \
	friend struct Z_Construct_UClass_ASpeedFood_Statics; \
public: \
	DECLARE_CLASS(ASpeedFood, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyTestProject"), NO_API) \
	DECLARE_SERIALIZER(ASpeedFood) \
	virtual UObject* _getUObject() const override { return const_cast<ASpeedFood*>(this); }


#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpeedFood(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpeedFood) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedFood); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedFood); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedFood(ASpeedFood&&); \
	NO_API ASpeedFood(const ASpeedFood&); \
public:


#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeedFood(ASpeedFood&&); \
	NO_API ASpeedFood(const ASpeedFood&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeedFood); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeedFood); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpeedFood)


#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_PRIVATE_PROPERTY_OFFSET
#define MyTestProject_Source_MyTestProject_SpeedFood_h_10_PROLOG
#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_PRIVATE_PROPERTY_OFFSET \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_SPARSE_DATA \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_RPC_WRAPPERS \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_INCLASS \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyTestProject_Source_MyTestProject_SpeedFood_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_PRIVATE_PROPERTY_OFFSET \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_SPARSE_DATA \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_INCLASS_NO_PURE_DECLS \
	MyTestProject_Source_MyTestProject_SpeedFood_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYTESTPROJECT_API UClass* StaticClass<class ASpeedFood>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyTestProject_Source_MyTestProject_SpeedFood_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
