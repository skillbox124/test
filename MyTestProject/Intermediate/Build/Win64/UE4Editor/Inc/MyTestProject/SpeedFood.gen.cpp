// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyTestProject/SpeedFood.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeedFood() {}
// Cross Module References
	MYTESTPROJECT_API UClass* Z_Construct_UClass_ASpeedFood_NoRegister();
	MYTESTPROJECT_API UClass* Z_Construct_UClass_ASpeedFood();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MyTestProject();
	MYTESTPROJECT_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASpeedFood::StaticRegisterNativesASpeedFood()
	{
	}
	UClass* Z_Construct_UClass_ASpeedFood_NoRegister()
	{
		return ASpeedFood::StaticClass();
	}
	struct Z_Construct_UClass_ASpeedFood_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeedFood_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MyTestProject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedFood_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpeedFood.h" },
		{ "ModuleRelativePath", "SpeedFood.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeedFood_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeedFood, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeedFood_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeedFood>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpeedFood_Statics::ClassParams = {
		&ASpeedFood::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeedFood_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedFood_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeedFood()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpeedFood_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpeedFood, 1997163322);
	template<> MYTESTPROJECT_API UClass* StaticClass<ASpeedFood>()
	{
		return ASpeedFood::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpeedFood(Z_Construct_UClass_ASpeedFood, &ASpeedFood::StaticClass, TEXT("/Script/MyTestProject"), TEXT("ASpeedFood"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeedFood);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
